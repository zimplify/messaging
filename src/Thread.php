<?php
    namespace Zimplify\Messaging;
    use Zimplify\Messaging\{Message, NotAlterableException};
    use Zimplify\Core\Application;
    use \DateTime;
    use \DateInterval;

    /**
     * the Thread is the core of BBS and in-app P2P message
     * @package Zimplify\Messaging (code 03)
     * @type instance (code 01)
     * @file Thread (code 03)
     */
    class Thread extends Message {

        const DEF_SHT_NAME = "core-msg::thread";
        const DEF_BROADCAST = "all";
        const ERR_NOT_ALLOWED = 403030103001;
        const FLD_CATEGORY = "category";
        const FLD_REPLIES = "replies";

        /**
         * overriding the default magical set
         * @param string $param the parameter to set
         * @param mixed $value the value to set
         * @return void
         */
        public function __set(string $param, $value) {
            switch ($param) {
                case self::FLD_REPLIES:
                    parent::__set($param, $value);
                    break;
                default:
                    if ($this->isUnique()) 
                        parent::__set($param, $value);
                    else 
                        throw new NotAlterableException("Alternation on thread is not allow unless is for replies.");
            }
        }

        /**
         * check if the sender is allow to post a reply
         * @param Agent $sender the sender of the message
         * @param array $receipients (reference) the receipients
         * @return bool
         */
        protected function isAllowed(Agent $sender, array &$receipients) : bool {
            $receipients = $this->{self::FLD_RECEIPIENTS};
            $result = false;

            // if is not a broadcast...
            if (!in_array(self::DEF_BROADCAST, $receipients))

                // since we know who is asking, and swap the receipients
                if (($i = array_search($sender->id, $receipients)) >= 0) {
                    array_splice($receipients, $i, 1, $this->parent()->id);
                    $result = true;
                }
            else 
                return true;

            // return resukt
            return $result;
        }

        /**
         * we need to override our original package since we need to save replies.
         * @return void
         */
        protected function package() : vpid {
            // do nothing here
            return true;
        }

        /**
         * adding the message reply to the parent message
         * @param Agent $sender the person who sends the message back
         * @param array $message the details of a message, should have the body and attachments
         * @return Thread
         */
        public function reply(Agent $sender, array $message, array $files = []) : self {
            // remake our receipient list
            $receipients = [];

            //  now make sure the person can answer
            if ($this->isAllowed($sender, $receipients)) {

                // creating the thread
                $reply = Application::create(self::DEF_SHT_NAME, $sender);
                $reply->populate($message);
                foreach ($files as $file) $reply->attach($file);
                $reply->receipients = $receipients;
                $reply->save();

                // binding the replies
                $replies = $this->replies;
                array_push($replies, $reply->id);
                $this->replies = $replies;
                return $this;
            } else 
                throw new RuntimeException("Sender is not allow to reply/", self::ERR_NOT_ALLOWED);
        }
    }

