<?php
    namespace Zimplify\Messaging\Activities;
    use Zimplify\Core\{Application, Task};
    use Zimplify\Core\Services\DataUtils;
    use Zimplify\Messaging\Email;
    use \RuntimeException;

    /**
     * the Thread Send activity triggers a new thread (in-app message)
     * @package Zimplify\Messaging (code 03)
     * @type Activity (code 05)
     * @file ThreadSend (code 02)
     */    
    class ThreadSend extends Task {

        const CLS_MESSAGE = "core-msg::thread";

        /**
         * check if the function is ready to begin.
         * @return bool
         */
        protected function isRequired() : bool {
            return $this->target && ($this->template || ($this->title && $this->body));
        }

        /**
         * running the function internally
         * @param Document $source (referenced) the source to run
         * @param array $inputs (referenced) the step of the functio
         * @return mixed
         */
        protected function run(Document &$source, string &$status = null, array &$inputs = []) {
            // initialize
            $message = Application::create(self::CLS_MESSAGE, $source->handler() ?? $source->author());
            $targets = DataUtils::evaluate($this->target, $source, $inputs);

            // first make sure we have target
            if (!$targets) {
                $inputs["error"] = new RuntimeException("Unable to locate target");
            } else {
                $message->receipients = is_array($targets) ? $targets : [$targets];

                // decide what's going in the message
                if ($this->template) 
                    $message->render($this->template, $source);
                else {
                    $message->title = DataUtils::evaluate($this->title, $ource, $inputs);
                    $message->body = DataUtils::evaluate($this->body, $ource, $inputs);
                }

                // once we bird out
                $message->save();
            }

            // now we return
            return $inputs["body"];
        }

    }    
