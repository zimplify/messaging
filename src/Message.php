<?php
    namespace Zimplify\Messaging;
    use Zimplify\Core\{Application, File, Instance, Query};
    use Zimplify\Core\Services\{ClassUtils, DataUtils};
    use Zimplify\Messaging\{NoSenderException, NotAlterableException};
    use Zimplify\Security\Agent;
    use \RuntimeException;    

    /**
     * Message is the base class of this package
     * @package Zimplify\Messaging (code 03)
     * @type instance (code 01)
     * @file Message (code 01)
     */
    abstract class Message extends Instance {

        const DEF_APPLICATION = "app";        
        const DEF_OBJECT_BASE = "MGOB_DATA";
        const DEF_TEMPLATE = "template";
        const ERR_BAD_SENDER  = 400030101001;
        const ERR_BAD_RECEIPIENT = 400030101002;
        const ERR_BAD_ATTACHMENT = 404030101003;
        const EVENT_BEFORE_SAVE = "before-save";
        const FLD_ATTACHEMENTS = "attachments";
        const FLD_RECEIPIENTS = "receipients";

        /**
         * override for magic method get
         * @param string $param 
         * @return mixed
         */
        public function __get(string $param) {
            $result = null;
            switch ($param) {
                case self::FLD_RECEIPIENTS:
                    break;
                default:
                    $result = parent::__get($param);
            }
            return $result;
        }

        /**
         * attaching files to the message
         * @param string $name the nickname for the file
         * @param File $target the file to upload
         * @return Message
         */
        public function attach(string $name, File $target) : self {
            $uploads = $this->{self::FLD_ATTACHEMENTS};
            $uploads[$name] = $target;
            $this->{self::FLD_ATTACHEMENTS} = $uploads;
            return $this;
        }

        /**
         * get the base field 
         * @return string
         */
        protected function base() {
            return self::DEF_OBJECT_BASE;
        }

        /**
         * check if the file specified is infact exist.
         * @param File $attachment the file we have to check against
         * @return bool
         */
        private function confirm(File $attacment) : bool {
            $result = false;
            return false;
        }

        /**
         * making sure we immutablise the message
         * @return void
         */
        protected function pacakge() : void {
            if (!$this->isUnique()) 
                throw new NotAlterableException("Message is immutable");
        }

        /**
         * the overriding prepare for initialization
         * @return void
         */
        protected function prepare() {
            parent::prepare();
            if ($this->parent()) {
                if (!ClassUtils::is($this->parent(), Agent::DEF_CLS_NAME)) 
                    throw new \RuntimeException("Improper sender detected.", self::ERR_BAD_SENDER);

                // adding a  presave check against mutate non-unique message
                $this->envoke(self::EVENT_BEFORE_SAVE, array($this, "package"));
            } else 
                throw new NoSenderException("Sender is not defined");
        }

        /**
         * overriding the default presave
         * @return void
         */
        protected function presave() {
            parent::presave();

            // need to validate the receipient are for real
            foreach ($this->{self::FLD_RECEIPIENTS} as $receipient) 
                if (!$this->validate($receipient))  
                    throw new RuntimeException("$receipient is not a valid receiver.", self::ERR_BAD_RECEIPIENT);        

            // let's try to validate our attachments too
            foreach ($this->{self::FLD_ATTACHMENTS} as $attachment) 
                if (!$this->confirm($attachment))
                    throw new RuntimeException("Attachment specified is not found.", self::ERR_BAD_ATTACHMENT);            
        }

        /**
         * rendering the message based on a template provided
         * @param string $template the template of to select from
         * @param Instance $datasource the data we are going to extract from
         * @param string $lang (optional) the language to base on render
         * @return Message
         */
        public function render(string $template, Instance $datasource, string $lang = "en") : self {
            foreach (json_decode(Application::fetch($template."-".$lang, self::DEF_APPLICATION, self::DEF_TEMPLATE), true) ?? [] as $section => $value) 
                $this->{$section} = $this->replace($value, $datasource);            
            return $this;
        }

        /**
         * replacing our data set with syntax along with regex
         * @param string $data our source data
         * @param Instance $datasource the data source we are looking at
         * @return string
         */
        private function replace(string $data, Instance $datasource) : string {            
            $matches = []; preg_match_all("/{{.*?}}/", $data, $matches);
            if (count($matches) > 0)
                foreach ($matches[0] as $match)
                    $data = str_replace($match, DataUtils::evalute(str_replace("{{", "", str_replace("}}", "", $match)), $dataousrce));
            return $data;
        }

        /**
         * making sure the target is a validate receipient for message
         * @param string $target the receipient
         * @param mixed $instance (referenced) the instance to send back on the user
         * @return bool
         */
        protected abstract function validate(string $target, &$instance = null) : bool;
    }