<?php
    namespace Zimplify\Messaging;
    use Zimplify\Messaging\Thread;
    use Zimplify\Messaging\Interaces\ISendableInterface;

    /**
     * the Email extends Thread to be able to SMTP out to outside world
     * @package Zimplify\Messaging (code 03)
     * @type instance (code 01)
     * @file Email (code 04)
     */
    class Email extends Thread {
    }