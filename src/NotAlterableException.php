<?php
    namespace Zimplify\Messaging;
    use \Exception;

    /**
     * This exception cater for when notification or immutable parts are changed
     * @package Zimplify\Messaging (code 03)
     * @type instance (code 11)
     * @file Notification (code 06)
     */    
    final class NotAlterableException extends Exception {

        /**
         * starting up the instance
         * @param string $message (optional) the message to let the user know what happened
         * @param Throwable $previous (optional) the exception triggered this one.
         * @return void
         */
        function __construct (string $message = "" , Throwable $previous = null) {
            parent::__construct($message, 500, $previous);
        }
    }