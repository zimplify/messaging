<?php
    namespace Zimplify\Messaging;
    use Zimplify\Core\{Application, Query};
    use Zimplify\Core\Services\ClassUtils;
    use Zimplify\Security\{Agent};
    use Zimplify\Messaging\Message;
    use \RuntimeException;

    /**
     * Notification is an immutable message that only use for display
     * @package Zimplify\Messaging (code 03)
     * @type instance (code 01)
     * @file Notification (code 02)
     */    
    class Notification extends Message {

        const DEF_ALL_USERS = "all";
        const FLD_CONTACT_NAME = "contact.name";
        const FLD_CONTACT_EMAIL = "contact.email";
    
        /**
         * the override over the Message magic get
         * @param string $param
         * @return mixed
         */
        public function __get(string $param) {
            $result = null;
            switch ($param) {
                case self::FLD_RECEIPIENTS:
                    $result = [];
                    if (in_array(self::DEF_ALL_USERS, $this->{self::FLD_RECEIPIENTS}))
                        array_push($result, ["me", ""]);
                    else 
                        foreach ($this->{self::FLD_RECEIPIENTS} as $receipient) {                            
                            $receiver = null;
                            if ($this->validate($rceipient, $receiver)) {
                                $name = $receiver->{self::FLD_CONTACT_NAME};
                                $email = $receiver->{self::FLD_CONTACT_EMAIL};
                                array_push($result, [implode(" ", $name), $email]);
                            }                            
                        }
                    break;
                default: 
                    $result = parent::__get($param);
            }
            return $result;
        }

        /**
         * making sure the target is a validate receipient for message
         * @param string $target the receipient
         * @param mixed $instance (referenced) the instance to send back on the user
         * @return bool
         */
        protected function validate(string $target, &$instance = null) : bool {
            $candidate = Application::search([Query::SRF_ID => $target]);
            if (count($candidate) == 1 && ClassUtils::is($candidate[0], Agent::DEF_CLS_NAME)) {
                $instance = $candidate[0];
                return true;
            } else 
                return false;
        }        
    }