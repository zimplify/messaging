<?php
    namespace Zimplify\Messaging;
    use \Exception;

    /**
     * This exception cater for when no sender information provided
     * @package Zimplify\Messaging (code 03)
     * @type instance (code 11)
     * @file Notification (code 05)
     */
    final class NoSenderException extends Exception {

        /**
         * starting up the instance
         * @param string $message (optional) the message to let the user know what happened
         * @param Throwable $previous (optional) the exception triggered this one.
         * @return void
         */
        function __construct (string $message = "" , Throwable $previous = null) {
            parent::__construct($message, 404, $previous);
        }
    }