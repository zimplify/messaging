<?php
    namespace Zimplify\Messaging;
    use Zimplify\Core\Application;
    use Zimplify\Messaging\{Message};
    use Zimplify\Rest\Controller;    
    use Zimplify\Security\{Agent};
    use Zimplify\Security\Interfaces\IAgencyInterface;
    use \RuntimeException;

    class ThreadCreateController extends Controller implements IAgencyInterface {

        const CLS_MESSAGE = "core-msg::thread";
                
        /**
         * this is the real function that processes the work
         * @param Request $req the request that triggers the work
         * @param array $args (optional) the values got from the URL dissect
         * @return Reply 
         */
        protected function process(Request $req, array $args = []) : Reply {
            $agent = $req->getAttribute(self::ATTR_AGENT);

            // creating our message
            $message = Application::create(self::CLS_MESSAGE, $agent);
            $message->populate($this->body);
            foreach ($this->files as $file) $message->attach($file);
            $message->save();

            // now get us a response
            return (new Reply())->withStatus(self::RES_ACCEPTED)->withJson(static::display($message, $agent));
        }

    }