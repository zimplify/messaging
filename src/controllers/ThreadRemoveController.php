<?php
    namespace Zimplify\Messaging;
    use Zimplify\Core\{Application, Query};
    use Zimplify\Messaging\{Message};
    use Zimplify\Rest\Controller;
    use Zimplify\Security\{Agent, UnauthorizedException} ;
    use Zimplify\Security\Interfaces\IAgencyInterface;
    use \RuntimeException;

    class ThreadReplyController extends Controller implements IAgencyInterface {

        const ARGS_MESSAGE = "msg";
        const CLS_MESSAGE = "core-msg::thread";
        const ERR_MSG_NOT_FOUND = 4040304004001;
        
        /**
         * this is the real function that processes the work
         * @param Request $req the request that triggers the work
         * @param array $args (optional) the values got from the URL dissect
         * @return Reply 
         */
        protected function process(Request $req, array $args = []) : Reply {
            $agent = $req->getAttribute(self::ATTR_AGENT);
            $message = array_key_exists(self::ARGS_MESSAGE, $args) ? $args[self::ARGS_MESSAGE] : null;
            
            // we need to load the message
            if ($message) {
                $messages = Application::search([Query::SRF_TYPE => self::CLS_MESSAGE]);
                if (count($messages) > 0) {
                    $target =  $messages[0];

                    // only do this if they recieve the message in the first place
                    if (($index = array_search($agent->id, $target->{Message::FLD_RECEIPIENTS})) >= 0) {                        
                        array_splice(($receipients = $target->{Message::FLD_RECEIPIENTS}), $index, 1);
                        $target->{Message::FLD_RECEIPIENTS} = $receipients;
                        $target->save();
                        
                        // now send back the response
                        return (new Reply())->withStatus(self::RES_SUCCESS)->withJson(null);
                    } else 
                        throw new UnauthorizedException("You are not in the receipient list.");
                } else 
                    throw new RuntimeException("Message is not available.", self::ERR_MSG_NOT_FOUND);
            } else 
                throw new RuntimeException("Cannot locate concerned message.", self::ERR_MSG_NOT_FOUND);

        }
    }