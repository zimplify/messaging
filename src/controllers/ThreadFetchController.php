<?php
    namespace Zimplify\Messaging;
    use Zimplify\Core\{Application, Query};
    use Zimplify\Messaging\{Message};
    use Zimplify\Rest\Controller;
    use Zimplify\Security\Agent;
    use Zimplify\Security\Interfaces\IAgencyInterface;
    use \RuntimeException;

    class ThreadReplyController extends Controller implements IAgencyInterface {

        const CLS_MESSAGE = "core-msg::thread";
        const DEF_EVERYBODY = "all";

        /**
         * this is the real function that processes the work
         * @param Request $req the request that triggers the work
         * @param array $args (optional) the values got from the URL dissect
         * @return Reply 
         */
        protected function process(Request $req, array $args = []) : Reply {
            $agent = $req->getAttribute(self::ATTR_AGENT);
            $query = [
                Query::SRF_TYPE => self::CLS_MESSAGE, 
                Query::SRF_STATUS => true,
                self::FLD_RECEIPIENTS => [Query::SRC_BINDER => "or", Query::SRC_EVALUATE => "ct", Query::SRC_VALUE => $agent->id],
                self::FLD_RECEIPIENTS => [Query::SRC_BINDER => "or", Query::SRC_EVALUATE => "ct", Query::SRC_VALUE => self::DEF_EVERYBODY]
            ];
            $data = [];

            // loading the data
            foreach (Application::search($query, "messages") as $message) 
                array_push($data, static::display($message, $agent));
            
            // now send the reply
            return (new Reply())->withJson($data);
        }
    }