<?php
    use Zimplify\Messaging\Controllers;
    use Zimplify\Core\{Application, Query};
    use Zimplify\Rest\Controller;
    use Zimplify\Security\Interfaces\IAgencyInterface;
    use \RuntimeException;

    /**
     * Controller on issuing new notification
     * @package Zimplify\Messaging (code 03)
     * @type Controller (code 04)
     * @file NotificationCreateController (code 01)
     */
    class NotificationCreateController extends Controller implements IAgencyInterface {

        const CFG_MAX_SIZE = "system.communications.notifications..output-size";
        const CLS_MESSAGE = "core-mesg::notification";
        const DEF_EVERYONE = "all";
        const FLD_EXPIRY = "expiry";
    
        /**
         * this is the real function that processes the work
         * @param Request $req the request that triggers the work
         * @param array $args (optional) the values got from the URL dissect
         * @return Reply 
         */
        protected function process(Request $req, array $args = []) : Reply {
            $agent = $req->getAttribute(self::ATTR_AGENT);
            $messages = Application::search([
                Query::SRF_TYPE => self::CLS_MESSAGE, 
                Query::SRF_STATUS => true,
                self::FLD_EXIPRY => [Query::SRC_EVALUATE => "gt", Query::SRC_VALUE => (new DateTime())->format("U")],
                Message::FLD_RECEIPIENTS => [Query::SRC_BIND => "or", Query::SRC_EVALUATE => "ct", Query::SRC_VALUE => $agent->id], 
                Message::FLD_RECEIPIENTS => [Query::SRC_BIND => "or", Query::SRC_EVALUATE => "ct", Query::SRC_VALUE => self::DEF_EVERYONE], 
            ]);
            
            // now we have to deal with the output
            $messages = array_reverse($messages);
            $data = [];
            $size = Application::env(self::CFG_MAX_SIZE);

            // we need to do a limited count
            for ($c = 0; $c < (count($messages) > $size ? $size : count($messages)) - 1; $c++) 
                array_push($data, static::display($messages[$c], $agent));

            // now build the result and return
            $result = new Reply();
            $result->withJson($data);
            return $result;
        }
    }