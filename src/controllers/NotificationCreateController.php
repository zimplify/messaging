<?php
    use Zimplify\Messaging\Controllers;
    use Zimplify\Rest\Controller;
    use Zimplify\Security\Interfaces\IAgencyInterface;
    use \RuntimeException;

    /**
     * Controller on issuing new notification
     * @package Zimplify\Messaging (code 03)
     * @type Controller (code 04)
     * @file NotificationCreateController (code 01)
     */
    class NotificationCreateController extends Controller implements IAgencyInterface {

        const CLS_MESSAGE = "core-msg::notification";
        const DEF_FILE_PREX = "attachment";

        /**
         * this is the real function that processes the work
         * @param Request $req the request that triggers the work
         * @param array $args (optional) the values got from the URL dissect
         * @return Reply 
         */
        protected function process(Request $req, array $args = []) : Reply {
            $agent = $req->getAttribute(self::ATTR_AGENT);
            $body = $this->getParsedBody();

            // getting our notifications
            $message = Application::create(self::CLS_MESSAGE, $agent);
            $message->populate($body);

            // save and return
            $result = new Reply();
            $result->withStatus(202)->withJson(static::display($message->save()));
            return $result;
        }
    }