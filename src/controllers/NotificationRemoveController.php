<?php
    namespace Zimplify\Messaging;
    use Zimplify\Core\{Application, Query};
    use Zimplify\Rest\Controller;
    use Zimplify\Messaging\{Message};
    use Zimplify\Security\{Agent, UnauthorizedException} ;
    use Zimplify\Security\Interfaces\IAgencyInterface;
    use \RuntimeException;

    class NotificationremoveController extends Controller implements IAgencyInterface {

        const ARGS_MESSAGE = "msg";
        const ERR_MSG_NOT_FOUND = 404030407001;
        const ERR_NOT_DONE = 500030407002;

        /**
         * this is the real function that processes the work
         * @param Request $req the request that triggers the work
         * @param array $args (optional) the values got from the URL dissect
         * @return Reply 
         */
        protected function process(Request $req, array $args = []) : Reply {
            $agent = $req->getAttribute(self::ATTR_AGENT);

            // locate the message
            if (!array_key_exists(self::ARGS_MESSAGE, $args)) 
                throw new RuntimeException("Cannot identify message to load", self::ERR_MSG_NOT_FOUND);
            $message = Application::load($args[self::ARGS_MESSAGE]);

            // now do some quick check
            if (!($message->parent()->id == $agent->id || ClassUtils::is($agent, self::CLS_ADMIN)))
                throw new UnauthorizedException("User is not permitted on this task");
            
            // now do the real work
            if ($message->delete()) 
                return (new Reply())->withStatus(self::RES_ACCEPTED)->withJson(null);
            else 
                throw new RuntimeException("Unable to complete request.", self::ERR_NOT_DONE);
        }
    }