<?php
    namespace Zimplify\Messaging;
    use Zimplify\Core\{Application, Provider};
    use Zimplify\Messaging\Message;
    use Zimplifu\Messaging\Interfaces\ISendableInterface;

    /**
     * the push provider make sure the push notification is send through the correct provider out to enduser
     * @package Zimplify\Messaging (code 03)
     * @type provider (code 3)
     * @file EmailProvider (code 02)
     */    
    class PushProvider extends Provider implements ISendableInterface {

        const CFG_PUSH_ENGINE = "system.providers.push";
        const ERR_BAD_CONFIG = 500030302001;
        const FLD_ENGINE = "engine";
        const FLD_CREDENTIAL = "credentials";
        const FLD_SETUP = "setup";

        /**
         * this is our detection script to ensure getting the right provider
         * @param array $setup the details on the engine
         * @return Provider
         */
        protected function detect(array $setup) : Provider {
            $engine = array_key_exists(self::FLD_ENGINE, $setup) ? $setup[self::FLD_ENGINE] : null;
            $credential = array_key_exists(self::FLD_CREDENTIAL, $setup) ? $setup[self::FLD_CREDENTIAL] : null;
            $this->debug("E: $engine, C: ".json_encode($credential));
            
            // now let's create
            if ($engine && $credential) {
                $result = Application::request($engine, [self::FLD_SETUP => $credential]);
                return $result;
            } else
                throw new RuntimeException("Required configuration is not available.", self::ERR_BAD_CONFIG);
        }

        /**
         * sending out the message
         * @param Message $message the message to send out
         * @return bool
         */
        public function send(Message $message) : bool {
            return $this->detect(Application::env(self::CFG_PUSH_ENGINE))->send($message);
        }      
    }