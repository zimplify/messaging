<?php
    namespace Zimplify\Messaging;
    use Zimplify\Core\{Applicaton, Provider};
    use Zimplify\Messaging\Email;
    use \RuntimeException;

    /**
     * the Email provider make sure the email is send through the correct provider out to enduser
     * @package Zimplify\Messaging (code 03)
     * @type provider (code 3)
     * @file EmailProvider (code 01)
     */
    class EmailProvider extends Provider {

        const CFG_EMAIL_ENGINE = "system.providers.smtp";
        const ERR_BAD_CONFIG = 50003301001;
        const FLD_CREDENTIAL = "credentials";
        const FLD_ENGINE = "engine";
        const FLD_SETUP = "setup";

        /**
         * detecting the engine to use for sending out email
         */
        protected function detect(array $setup) : Provider {
            $engine = array_key_exists(self::FLD_ENGINE, $setup) ? $setup[self::FLD_ENGINE] : null;
            $credential = array_key_exists(self::FLD_CREDENTIAL, $setup) ? $setup[self::FLD_CREDENTIAL] : null;
            $this->debug("E: $engine, C: ".json_encode($credential));
            
            // now let's create
            if ($engine && $credential) {
                $result = Application::request($engine, [self::FLD_SETUP => $credential]);
                return $result;
            } else
                throw new RuntimeException("Required configuration is not available.", self::ERR_BAD_CONFIG);
        }

        /**
         * startup initializer for the service
         * @return void
         */
        protected function initialize() {
        }

        /**
         * check if all startup arguments are available
         * @return bool
         */
        protected function isRequired() : bool {
            return true;
        }

        /**
         * sending out the email messag
         * @param Email $message the message to send
         */
        public function send(Email $message) : bool {        
            return $this->detect(Application::env(self::CFG_EMAIL_ENGINE))->send($message);
        }
    }