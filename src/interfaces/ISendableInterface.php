<?php
    namespace Zimplify\Messaging\Interfaces;
    use Zimplify\Messaging\Message;

    /**
     * this is a contractual interface for email sending service provider
     * @package Zimplify\Messaging (code 03)
     * @type interface (code 06)
     * @file ISendableInterface (code 01)
     */
    interface ISendableInterface {

        /**
         * sending out the message
         * @param Message $message the message to send out
         * @return bool
         */
        function send(Message $message) : bool;
    }